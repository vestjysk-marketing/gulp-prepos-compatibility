var PLUGIN_NAME = 'prepos-compatibility';
var through     = require('through2');
var fs          = require('fs');

module.exports = function () {
	return through.obj(function (chunk, encoding, cb) {
		if (chunk.isStream()) {
			return cb('Streams not supported in ' + PLUGIN_NAME);
		}

		var content = chunk.contents.toString(encoding);

		var regex = /\s*@prepros-(prepend|append)\s(.*\.js)/g;

		var prepend = '';
		var append  = '';
		while (match = regex.exec(content)) {
			var path = chunk.base;
			if (path.substr(-1, 1) !== '/' && path.substr(-1, 1) !== '\\') {
				path += '/';
			}
			path += match[2];

			if (match[1] == 'prepend') {
				prepend += fs.readFileSync(path, encoding);
			} else if (match[1] == 'append') {
				append += fs.readFileSync(path, encoding);
			}
		}

		prepend        = new Buffer(prepend);
		append         = new Buffer(append);
		chunk.contents = Buffer.concat([prepend, chunk.contents, append]);

		return cb(null, chunk);
	});
}
